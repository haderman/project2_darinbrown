var express = require('express');
var router = express.Router();

var property_dal = require('../model/property_dal')


router.get('/all', function(req, res) {
    property_dal.getAll(function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.render('property/propertyViewAll', { 'result':result });
        }
    });

});



module.exports = router;